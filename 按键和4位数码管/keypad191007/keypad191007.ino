#include <TM1637.h>
#include <Wire.h>

#include <Keypad.h>    // 引用Keypad程式庫
#define CLK 3  
#define DIO 2
TM1637 tm1637(CLK, DIO);
int8_t TimeDisp[] = {0x00,0x00,0x00,0x00};
int fingerid;
#define KEY_ROWS 4 // 按鍵模組的列數
#define KEY_COLS 4 // 按鍵模組的行數
 int keyflag=0;
// 依照行、列排列的按鍵字元（二維陣列）
char keymap[KEY_ROWS][KEY_COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
int id;

byte colPins[KEY_COLS] = {9, 8, 7, 6};     // 按鍵模組，行1~4接腳。
byte rowPins[KEY_ROWS] = {13, 12, 11, 10}; // 按鍵模組，列1~4接腳。 

// 初始化Keypad物件
// 語法：Keypad(makeKeymap(按鍵字元的二維陣列), 模組列接腳, 模組行接腳, 模組列數, 模組行數)
Keypad myKeypad = Keypad(makeKeymap(keymap), rowPins, colPins, KEY_ROWS, KEY_COLS);

void setup(){
  Serial.begin(9600);
    tm1637.init();
/*     tm1637.set(1);*/
 

}
  
int keypad(){
  // 透過Keypad物件的getKey()方法讀取按鍵的字元
  char key = myKeypad.getKey();

 if (key){

    
//按下*键，则清0
 if(key=='*') {
      TimeDisp[0]=0;
      TimeDisp[1]=0;
      TimeDisp[2]=0;
      TimeDisp[3]=0; 
      keyflag=0; 
      
//      Serial.print(TimeDisp[3]);
//      Serial.print(TimeDisp[2]);
//      Serial.print(TimeDisp[1]);
//      Serial.println(TimeDisp[0]);
      }

 else if(key=='#') {
//按下#，则将TimeDisp的值赋给fingerid并打印 
//   for(4;keyflag>0;keyflag--)
//   {   
//    TimeDisp[keyflag-4] = int(key)-48;//把char转化为int类型，然后变成ascII码，再-48
//Serial.println(TimeDisp[keyflag-4]);

    fingerid=TimeDisp[3]*1000+TimeDisp[2]*100+TimeDisp[1]*10+TimeDisp[0]; 
    TimeDisp[0]=0;
    TimeDisp[1]=0;
    TimeDisp[2]=0;
    TimeDisp[3]=0; 
//      tm1637.display(TimeDisp);

//    tm1637.display(0, 0);
    Serial.print("***********fingerid=");
    Serial.println(fingerid);
    }
    else{
//      if(key!='#'&&key!='*'){
   // 如果不是按下#且*,则keyflag++,如果keyflag<=4则把输入的值赋给TimeDisp[]
    keyflag++;
    Serial.print("***********key");
    Serial.println(key);
    if(4==keyflag){
    TimeDisp[0]=int(key)-48;}
    else if(3==keyflag){
    TimeDisp[1]=int(key)-48;}
    else if(2==keyflag){
    TimeDisp[2]=int(key)-48;}
    else if(1==keyflag){
    TimeDisp[3]=int(key)-48;}

//    Serial.print(TimeDisp[3]);
//    Serial.print(TimeDisp[2]);
//    Serial.print(TimeDisp[1]);
//    Serial.println(TimeDisp[0]);
    }
      tm1637.display(TimeDisp);
    }
    return fingerid;//返回fingerid
}

void loop(){
  id = keypad();//调用keypad值，赋给id

  if (id == 0) {// ID #0 not allowed, try again!
  Serial.print("Enrolling ID #");
  }
  else
  Serial.println(id);
  }
