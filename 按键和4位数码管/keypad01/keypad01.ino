#include <TM1637.h>
#include <Wire.h>

#include <Keypad.h>    // 引用Keypad程式庫
#define CLK 3  
#define DIO 2
TM1637 tm1637(CLK, DIO);
int8_t TimeDisp[] = {0x00,0x00,0x00,0x00};

#define KEY_ROWS 4 // 按鍵模組的列數
#define KEY_COLS 4 // 按鍵模組的行數

// 依照行、列排列的按鍵字元（二維陣列）
char keymap[KEY_ROWS][KEY_COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

byte colPins[KEY_COLS] = {9, 8, 7, 6};     // 按鍵模組，行1~4接腳。
byte rowPins[KEY_ROWS] = {13, 12, 11, 10}; // 按鍵模組，列1~4接腳。 

// 初始化Keypad物件
// 語法：Keypad(makeKeymap(按鍵字元的二維陣列), 模組列接腳, 模組行接腳, 模組列數, 模組行數)
Keypad myKeypad = Keypad(makeKeymap(keymap), rowPins, colPins, KEY_ROWS, KEY_COLS);

void setup(){
  Serial.begin(9600);
    tm1637.init();
  tm1637.set(1);

}
  
void loop(){
  // 透過Keypad物件的getKey()方法讀取按鍵的字元
  char key = myKeypad.getKey();

  if (key){  // 若有按鍵被按下…
//        tm1637.display(0, key);
  TimeDisp[3] = int(key)-48;
Serial.println(TimeDisp[3]);
    tm1637.display(TimeDisp);

//  tm1637.display(1, b1);
//  tm1637.display(2, b2);
//  tm1637.display(3, b3);
//Serial.println(key);  // 顯示按鍵的字元
  }
}
