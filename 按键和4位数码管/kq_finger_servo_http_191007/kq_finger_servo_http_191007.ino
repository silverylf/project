/*************************************************** 
  This is an example sketch for our optical Fingerprint sensor

  Designed specifically to work with the Adafruit BMP085 Breakout 
  ----> http://www.adafruit.com/products/751

  These displays use TTL Serial to communicate, 2 pins are required to 
  interface
  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ****************************************************/


#include <Adafruit_Fingerprint.h>
#include <Servo.h>
#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

/*****************数码管及keypad定义开始**************/
#include <TM1637.h>
#include <Wire.h>

#include <Keypad.h>    // 引用Keypad程式庫
#define CLK 3  
#define DIO 2
TM1637 tm1637(CLK, DIO);
uint8_t TimeDisp[] = {0x00,0x00,0x00,0x00};
int fingerid;
#define KEY_ROWS 4 // 按鍵模組的列數
#define KEY_COLS 4 // 按鍵模組的行數
 int keyflag=0;
// 依照行、列排列的按鍵字元（二維陣列）
char keymap[KEY_ROWS][KEY_COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
//int id;

byte colPins[KEY_COLS] = {9, 8, 7, 6};     // 按鍵模組，行1~4接腳。
byte rowPins[KEY_ROWS] = {13, 12, 11, 10}; // 按鍵模組，列1~4接腳。 

// 初始化Keypad物件
// 語法：Keypad(makeKeymap(按鍵字元的二維陣列), 模組列接腳, 模組行接腳, 模組列數, 模組行數)
Keypad myKeypad = Keypad(makeKeymap(keymap), rowPins, colPins, KEY_ROWS, KEY_COLS);
/*****************数码管及keypad定义结束**************/

#define PIN_SERVO D4
#define PIN_jq8900 D1
ESP8266WiFiMulti WiFiMulti;
Servo myservo;
//uint8_t kqid = 0;


// On Leonardo/Micro or others with hardware serial, use those! #0 is green wire, #1 is white
// uncomment this line:
// #define mySerial Serial1

// For UNO and others without hardware serial, we must use software serial...
// comment these two lines if using hardware serial
SoftwareSerial mySerial(D2, D3);
// pin #2 is IN from sensor (GREEN wire)
// pin #3 is OUT from arduino  (WHITE wire)

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&mySerial);
uint8_t id;

uint8_t readnumber(void) {    //??????????foxzenith190606
uint8_t num = 0;
uint8_t fingernums;
//uint8_t seconds;
//uint8_t second1;
 
  while (num == 0) {
    while (! Serial.available());
    num = Serial.parseInt();
  }
  return num;
}

unsigned char choose1[] = {0xAA, 0x07, 0x02, 0x00, 0x01, 0xB4,   };//第1首
unsigned char choose2[] = {0xAA, 0x07, 0x02, 0x00, 0x02, 0xB5,    };//第2首
unsigned char choose3[] = {0xAA, 0x07, 0x02, 0x00, 0x03, 0xB6,    };//第3首
unsigned char choose4[] = {0xAA, 0x07, 0x02, 0x00, 0x04, 0xB7,    };//第4首
unsigned char choose5[] = {0xAA, 0x07, 0x02, 0x00, 0x05, 0xB8,   };//第5首
unsigned char choose6[] = {0xAA, 0x07, 0x02, 0x00, 0x06, 0xB9,   };//第6首
unsigned char choose7[] = {0xAA, 0x07, 0x02, 0x00, 0x07, 0xBA,   };//第7首
unsigned char choose8[] = {0xAA, 0x07, 0x02, 0x00, 0x08, 0xBB,   };//第8首
unsigned char chooseall[] = {0xAA, 0x18, 0x01, 0x00, 0xC3,    };//全部循环
unsigned char loop3[] = {0xAA, 0x19, 0x02, 0x00, 0x03, 0xC8,     };//循环3次
unsigned char play[] = {0xAA, 0x02, 0x00, 0xAC,   };//播放

void jq8900_success() {
  Serial.write(choose1,6);
//  Serial.write(chooseall,5);
//  Serial.write(loop3,6);
//  Serial.write(play,4);
  delay(1000);
}

void jq8900_failed() {
  Serial.write(choose8,6);
//  Serial.write(chooseall,5);
//  Serial.write(loop3,6);
//  Serial.write(play,4);
  delay(3000);
}


void kq(uint8_t kqid) {  //发送指纹id函数
    // wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED)) {

        HTTPClient http;

        Serial.print("[HTTP] begin...\n");
        // configure traged server and url
        //http.begin("https://192.168.1.12/test.html", "7a 9c f4 db 40 d3 62 5a 6e 21 bc 5c cc 66 c8 3e a1 45 59 38"); //HTTPS
        //http.begin("http://118.190.215.49:5001/order?id=123&name=XiaoMing"); //HTTP
        //http.begin("http://101.132.176.206/index.php?id=123");
        http.begin("http://kq.lvguoyuan.xyz/index.php?id="+kqid);
        Serial.print("[HTTP] GET...\n");
        // start connection and send HTTP header
        int httpCode = http.GET();

        // httpCode will be negative on error
        if(httpCode > 0) {
            // HTTP header has been send and Server response header has been handled
            Serial.printf("[HTTP] GET... code: %d\n", httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
                Serial.println(payload);
            }
        } else {
            Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }

        http.end();
    }

    delay(3000);
}
/************kepad的实现方法**********/
int keypad(){
  // 透過Keypad物件的getKey()方法讀取按鍵的字元
  char key = myKeypad.getKey();

 if (key){

    
//按下*键，则清0
 if(key=='*') {
      TimeDisp[0]=0;
      TimeDisp[1]=0;
      TimeDisp[2]=0;
      TimeDisp[3]=0; 
      keyflag=0; 
      
//      Serial.print(TimeDisp[3]);
//      Serial.print(TimeDisp[2]);
//      Serial.print(TimeDisp[1]);
//      Serial.println(TimeDisp[0]);
      }

 else if(key=='#') {
//按下#，则将TimeDisp的值赋给fingerid并打印 
//   for(4;keyflag>0;keyflag--)
//   {   
//    TimeDisp[keyflag-4] = int(key)-48;//把char转化为int类型，然后变成ascII码，再-48
//Serial.println(TimeDisp[keyflag-4]);

    fingerid=TimeDisp[3]*1000+TimeDisp[2]*100+TimeDisp[1]*10+TimeDisp[0]; 
    TimeDisp[0]=0;
    TimeDisp[1]=0;
    TimeDisp[2]=0;
    TimeDisp[3]=0; 
//      tm1637.display(TimeDisp);

//    tm1637.display(0, 0);
    Serial.print("***********fingerid=");
    Serial.println(fingerid);
    }
    else{
//      if(key!='#'&&key!='*'){
   // 如果不是按下#且*,则keyflag++,如果keyflag<=4则把输入的值赋给TimeDisp[]
    keyflag++;
    Serial.print("***********key");
    Serial.println(key);
    if(4==keyflag){
    TimeDisp[0]=int(key)-48;}
    else if(3==keyflag){
    TimeDisp[1]=int(key)-48;}
    else if(2==keyflag){
    TimeDisp[2]=int(key)-48;}
    else if(1==keyflag){
    TimeDisp[3]=int(key)-48;}

//    Serial.print(TimeDisp[3]);
//    Serial.print(TimeDisp[2]);
//    Serial.print(TimeDisp[1]);
//    Serial.println(TimeDisp[0]);
    }
      tm1637.display(TimeDisp);
    }
    return fingerid;//返回fingerid
}
/*********keypad的实现方法**********/

void enroll()                     // run over and over again
{
  Serial.println("Ready to enroll a fingerprint!");
  Serial.println("Please type in the ID # (from 1 to 127) you want to save this finger as...");
//  id = readnumber();
id = keypad();//调用keypad值，赋给id
  
  if (id == 0) {// ID #0 not allowed, try again!
     return;
  }
  Serial.print("Enrolling ID #");
  Serial.println(id);
  
  while (!  getFingerprintEnroll() );
}


uint8_t getFingerprintEnroll() {

  int p = -1;
  Serial.print("Waiting for valid finger to enroll as #"); Serial.println(id);
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.println(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(1);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }
  
  Serial.println("Remove finger");
  delay(2000);
  p = 0;
  while (p != FINGERPRINT_NOFINGER) {
    p = finger.getImage();
  }
  Serial.print("ID "); Serial.println(id);
  p = -1;
  Serial.println("Place same finger again");
  while (p != FINGERPRINT_OK) {
    p = finger.getImage();
    switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      break;
    case FINGERPRINT_NOFINGER:
      Serial.print(".");
      break;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      break;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      break;
    default:
      Serial.println("Unknown error");
      break;
    }
  }

  // OK success!

  p = finger.image2Tz(2);
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }
  
  // OK converted!
  Serial.print("Creating model for #");  Serial.println(id);
  
  p = finger.createModel();
  if (p == FINGERPRINT_OK) {
    Serial.println("Prints matched!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_ENROLLMISMATCH) {
    Serial.println("Fingerprints did not match");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
  
  Serial.print("ID "); Serial.println(id);
  p = finger.storeModel(id);
  if (p == FINGERPRINT_OK) {
    Serial.println("Stored!");
//    fingernums=finger.getTemplateCount();
//    Serial.print("Current fingernums is: ");  Serial.println(fingernums);
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_BADLOCATION) {
    Serial.println("Could not store in that location");
    return p;
  } else if (p == FINGERPRINT_FLASHERR) {
    Serial.println("Error writing to flash");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
}

/*
void longpress() {
  uint8_t seconds;
  uint8_t second1;
  if (digitalRead(D5) == 1)
  {
    second1 = millis();
  }
  else
  {
    seconds = millis() - second1;
    if (seconds > 3000)
    {
      Serial.write(choose6,6);
      Serial.println("Start Enroll..........");
      enroll();
//      return 1;
    }
    else
    {
      uint8_t getFingerprintIDez();
      delay(50);            //don't ned to run this at full speed.
    }
//    else
//    {
//      return 0;  
//    }
  }
}*/

void setup()  
{
  pinMode(D5, INPUT_PULLUP);  //button
      tm1637.init();

  myservo.attach(PIN_SERVO);
  myservo.write(0);           //servo back to 0 degrees
  pinMode(D1, OUTPUT);        // D1 pin

  Serial.begin(9600);
  Serial.setDebugOutput(true);
  
    Serial.println();
    Serial.println();
    Serial.println();

    for(uint8_t t = 4; t > 0; t--) {
        Serial.printf("[SETUP] WAIT %d...\n", t);
        Serial.flush();
        delay(1000);
    }

    WiFi.mode(WIFI_STA);
    WiFiMulti.addAP("602iot", "18wulian");
  while (!Serial);  // For Yun/Leo/Micro/Zero/...
  delay(100);
  Serial.println("\n\nAdafruit finger detect test");

  // set the data rate for the sensor serial port
  finger.begin(57600);
  
  if (finger.verifyPassword()) {
    Serial.println("Found fingerprint sensor!");
  } else {
    Serial.println("Did not find fingerprint sensor :(");
    while (1) { delay(1); }
  }

  finger.getTemplateCount();
  Serial.print("Sensor contains "); Serial.print(finger.templateCount); Serial.println(" templates");
  Serial.println("Waiting for valid finger...");

  
}



void loop()                     // run over and over again
{
/*if(longpress())  //长按
{
  Serial.println("Start Enroll..........");
  enroll();
  delay(50);
}
else
{
  getFingerprintIDez();
  delay(50);            //don't ned to run this at full speed.
}
}*/
uint8_t seconds;
  uint8_t second1;
  if (digitalRead(D5) == 1)
  {
    second1 = millis();
  }
  else
  {
    seconds = millis() - second1;
    if (seconds > 3000)
    {
      Serial.write(choose6,6);
      Serial.println("Start Enroll..........");
      enroll();
//      return 1;
    }
//    else
//    {
//       getFingerprintIDez();
//      delay(50);            //don't ned to run this at full speed.
//    }
  }
  getFingerprintIDez();
  }

uint8_t getFingerprintID() {
  uint8_t p = finger.getImage();
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image taken");
      Serial.print("------------------");Serial.println(finger.fingerID);
      Serial.print("Current fingernums is: ");  Serial.println(finger.getTemplateCount());
      break;
    case FINGERPRINT_NOFINGER:
      Serial.println("No finger detected");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_IMAGEFAIL:
      Serial.println("Imaging error");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }

  // OK success!

  p = finger.image2Tz();
  switch (p) {
    case FINGERPRINT_OK:
      Serial.println("Image converted");
      break;
    case FINGERPRINT_IMAGEMESS:
      Serial.println("Image too messy");
      return p;
    case FINGERPRINT_PACKETRECIEVEERR:
      Serial.println("Communication error");
      return p;
    case FINGERPRINT_FEATUREFAIL:
      Serial.println("Could not find fingerprint features");
      return p;
    case FINGERPRINT_INVALIDIMAGE:
      Serial.println("Could not find fingerprint features");
      return p;
    default:
      Serial.println("Unknown error");
      return p;
  }
  
  // OK converted!
  p = finger.fingerFastSearch();
  if (p == FINGERPRINT_OK) {
    Serial.println("Found a print match!");
  } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
    Serial.println("Communication error");
    return p;
  } else if (p == FINGERPRINT_NOTFOUND) {
    Serial.println("Did not find a match");
    return p;
  } else {
    Serial.println("Unknown error");
    return p;
  }   
  
  // found a match!
  Serial.print("Found ID #"); Serial.print(finger.fingerID); 
  Serial.print(" with confidence of "); Serial.println(finger.confidence); 

  return finger.fingerID;
}


// returns -1 if failed, otherwise returns ID #
int getFingerprintIDez() {
  uint8_t p = finger.getImage();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.image2Tz();
  if (p != FINGERPRINT_OK)  return -1;

  p = finger.fingerFastSearch();
  if (p != FINGERPRINT_OK)  {
    jq8900_failed();
    return -1;
  }
  else{
  // found a match!
  Serial.print("Found ID #"); Serial.print(finger.fingerID); 
  Serial.print(" with confidence of "); Serial.println(finger.confidence);
    delay(1000);
  myservo.write(90);  //90 /180 依据实际情况设定
  jq8900_success();
  delay(5000);
  myservo.write(0);
  delay(1000);
  kq(finger.fingerID);
  return finger.fingerID; 
  }

}
