#include <dht.h>                //DHT11温湿度传感器
#include <Wire.h>               //1602LCD屏
#include <LiquidCrystal_I2C.h>  //I²C转接板

#define dht_pin 2  //温湿度传感器——2号IO针脚
#define heat 3     //空气加热器所用的继电器——3号针脚
//1#电机
#define ENA1 5 //控制PWM转速
#define IN1 7  //控制正反转
#define IN2 8  //控制正反转
//2#电机
#define ENA2 6 //控制PWM转速
#define IN3 9  //控制正反转
#define IN4 10 //控制正反转

/*声明函数*/
void forward1(void);  //1#电机正转调速
void forward2(void);  //2#电机正转调速
//void reverse1(void);  //1#电机反转调速
//void reverse2(void);  //2#电机反转调速
void brake1(void);    //1#电机刹车
void brake2(void);    //2#电机刹车

int Speed;  //用来映射PWM的值

//0x27是I²C的地址，可显示16列*2行的字符
LiquidCrystal_I2C lcd(0x27,16,2);  //0x27是LCD的地址，不行的话就换0x3F
dht DHT;  //创建一个变量指向对象

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  lcd.init();             //初始化lcd屏
  lcd.backlight();        //打开lcd背光
  lcd.setCursor(0,0);     //将光标定在0列0行
  lcd.print("    Welcome!    ");
  pinMode(heat, OUTPUT);
  
  motor_pinint(); //电机驱动初始化
  forward1(); //1#电机正转调速
  forward2(); //2#电机正转调速
  
  delay(1000);
}

void loop() {
  // put your main code here, to run repeatedly:
  /*使用DHT11*/
  DHT.read11(dht_pin);
//  /*使用DHT22*/
//  DHT.read22(dht_pin);
  
  Serprint();   //监视器显示温湿度
  //风扇转速：温度15-30℃映射PWM：100-255
  Speed = map(DHT.temperature, 15, 30, 100, 255);
  temctrl();    //温控风扇
  lcd.clear();  //LCD清屏
  lcdprint();   //LCD显示温湿度、PWM、空气加热器状态
  delay(1000);
}

/*电机驱动设置*/
void motor_pinint(void){
  /*1#电机*/
  pinMode(ENA1, OUTPUT);  //使能端，设置为输出模式
  pinMode(IN1, OUTPUT);   //IN1所接IO针脚设置为输出模式
  pinMode(IN2, OUTPUT);   //IN2所接IO针脚设置为输出模式
  /*2#电机*/
  pinMode(ENA2, OUTPUT);  //使能端，设置为输出模式
  pinMode(IN3, OUTPUT);   //IN3所接IO针脚设置为输出模式
  pinMode(IN4, OUTPUT);   //IN4所接IO针脚设置为输出模式
}

/*1#电机正转调速*/
void forward1(void){
  /*IN1=1(HIGH),IN2=0(LOW),1#电机正转*/
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
}

/*2#电机正转调速*/
void forward2(void){
  /*IN3=1(HIGH),IN4=0(LOW),2#电机正转*/
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
}

///*1#电机反转调速*/
//void reverse1(void){
//  /*IN1=0(LOW),IN2=1(HIGH),1#电机反转*/
//  digitalWrite(IN1, LOW);
//  digitalWrite(IN2, HIGH);
//}

///*2#电机反转调速*/
//void reverse2(void){
//  /*IN3=0(LOW),IN4=1(HIGH),2#电机反转*/
//  digitalWrite(IN3, LOW);
//  digitalWrite(IN4, HIGH);
//}

/*1#电机刹车*/
void brake1(void){
  /*IN1=0(LOW),IN2=0(LOW),1#电机刹车*/
  digitalWrite(IN1, LOW);  
  digitalWrite(IN2, LOW);
}

/*2#电机刹车*/
void brake2(void){
  /*IN3=0(LOW),IN4=0(LOW),2#电机刹车*/
  digitalWrite(IN3, LOW);  
  digitalWrite(IN4, LOW);
}

/*温控*/
void temctrl(void){
  if(DHT.temperature<15){
    digitalWrite(heat, LOW);  //低电平触发，打开空气加热器
    brake1(); //1#电机刹车
    brake2(); //2#电机刹车
//    analogWrite(ENA1, 0);     //风扇关
//    analogWrite(ENA2, 0);
  }//15-30℃温控转速
  else if(DHT.temperature>=15 && DHT.temperature<=30){
    digitalWrite(heat, HIGH); //关闭空气加热器
    forward1(); //1#电机正转调速
    forward2(); //2#电机正转调速
    analogWrite(ENA1, Speed); //温控风扇转速
    analogWrite(ENA2, Speed);
  }
  else{
    digitalWrite(heat, HIGH); //关闭空气加热器
    forward1(); //1#电机正转调速
    forward2(); //2#电机正转调速
    analogWrite(ENA1, 255);   //风扇最大功率
    analogWrite(ENA2, 255);    
  }
  Serial.print("Fan PWM: ");
  Serial.println(Speed);
}

/*监视器显示温湿度*/
void Serprint(void){
  Serial.print("temperature = "); //温度
  Serial.print(DHT.temperature);
  Serial.println("C  ");
  Serial.print("humidity = ");    //湿度
  Serial.print(DHT.humidity);
  Serial.println("%  ");
}

/*LCD显示温湿度、PWM、空气加热器状态*/
void lcdprint(void){
  //LCD显示，eg: En:25.00C/50.00%
  lcd.setCursor(0,0); //将光标定在0列0行
  lcd.print("En:");   //Environment
  lcd.print(DHT.temperature);
  lcd.print("C");     //接着输出单位
  lcd.print("/");
  lcd.print(DHT.humidity);
  lcd.print("%");

  //LCD: PWM:xxx Heat:[ON/OFF]
  lcd.setCursor(0,1); //将光标定在0列1行
  lcd.print("PWM:");
  lcd.print(Speed);
  lcd.setCursor(8,1); //将光标定在8列1行
  Serial.print("Heat state: ");
  lcd.print("Heat:");
  if(digitalRead(heat)==LOW){
//    Serial.println("ON");
    lcd.print("ON");
  }
  else{
//    Serial.println("OFF");
    lcd.print("OFF");  
  }
}
