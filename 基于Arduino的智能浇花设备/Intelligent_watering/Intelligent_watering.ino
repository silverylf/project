/***************************************************************************
 ********************************库文件引用********************************
 ***************************************************************************/
//I2C_LCD1602
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
//红外遥控
#include <IRremote.h>
//DS1302
#include <stdio.h>
#include <string.h>
#include <DS1302.h>
//DHT11
#include <dht.h>

/***************************************************************************/

/***************************************************************************
 *********************************变量定义*********************************
 ***************************************************************************/
//I2C_LCD1602
#define LCD_ROWS  2       //LCD的行数
#define LCD_COLS  16      //LCD的列數
LiquidCrystal_I2C lcd(0x27, LCD_COLS, LCD_ROWS);  //LCD的I2C地址一般为0x27或0x3F
boolean Current_ROWS = 0; //当前所在行数，用于时间设置界面的所在行切换
unsigned long Last_Page_Rotation_Time = 0;        //用于页面轮播的计时

//DS1302
#define CE_PIN    5       //RST
#define IO_PIN    6       //DAT
#define SCL_PIN   7       //CLK
char day[10];                         //日期变量缓存
int NumData[7] ={0}, j = 0;           //用于存储切割后的时间数据
DS1302 rtc(CE_PIN, IO_PIN, SCL_PIN);  //创建 DS1302 对象
unsigned long Timing = 0;             //用于时间显示计时切换
#define BUF_LEN         20
#define SERIAL_BUF_LEN  50
char SERIAL_BUF[SERIAL_BUF_LEN];      //存储在串口打印出来的时间
char LCD_BUF1[BUF_LEN];               //存储在LCD打印的数据
char LCD_BUF2[BUF_LEN];  

//DHT11
dht DHT;
#define DHT11_Pin 8                     //DHT: 8#引脚

//Soil
#define Soil_Pin A0                     //Sooil: A0#引脚
#define Soil_Read analogRead(Soil_Pin)  //读取当前土壤湿度的模拟量
unsigned int AD_Soil_Value = 0;         
unsigned int Soil_Percentage = 0;       //土壤湿度相对百分比

//Liquid_Level
#define Liquid_Level_Pin A1             //Liquid: A1#引脚
#define Liquid_Level_Read analogRead(Liquid_Level_Pin)  //读取液位传感器的模拟量
unsigned int AD_Liquid_Level_Value = 0;
unsigned int Level_Percentage = 0;      //液位相对百分比
#define Low     0
#define Midium  1
#define High    2
unsigned int Liquid_Flag = Low;
char Soil_Liquid_BUF1[BUF_LEN];         //存储在串口和LCD打印的土壤，液位，水泵数据
char Soil_Liquid_BUF2[BUF_LEN];

//Pump
#define Pump_Relay 2                              //Relay: 2#引脚
#define Pump_ON  digitalWrite(Pump_Relay, LOW)    //开水泵
#define Pump_OFF digitalWrite(Pump_Relay, HIGH)   //关水泵
#define Auto true
#define Maul false
boolean Pump_Mode = Auto;                         //水泵模式默认为自动控制

//Buzzer
#define Buzzer_Pin 3                              //蜂鸣器引脚为4#
#define Buzzer_ON digitalWrite(Buzzer_Pin, HIGH)  //打开蜂鸣器
#define Buzzer_OFF digitalWrite(Buzzer_Pin, LOW)  //关闭蜂鸣器

//IR_Control
#define irReceiver_Pin 4        //IR: 4#引脚
IRrecv irrecv(irReceiver_Pin);  //创建一个IRrecv对象
decode_results results;
String InputCode = "";          //暂存用户的按键字符串

/**************************************************
 * CH- :FFA25D      CH :FF629D          CH+ :FFE21D
 *   清除字符        设定/确认时间           退出时间设置
 * PREV:FF22DD     NEXT:FF02FD    PLAY/PAUSE:FFC23D
 *   上一页            下一页        开启/取消5秒自动切换
 *   - :FFE01F       + :FFA857           EQ :FF906F
 *   手动关水泵        手动开水泵     开启/关闭水泵自动模式
 **************************************************/
#define CH_L  0xFFA25D  //CH- :清除字符
#define CH    0xFF629D  //CH  :设定时间
#define CH_R  0xFFE21D  //CH+ :退出时间设置界面
#define PREV  0xFF22DD  //PREV:上一页
#define NEXT  0xFF02FD  //NEXT:下一页
#define PLAY  0xFFC23D  //PLAY:自动/手动播放模式切换
#define Minus 0xFFE01F  // -  :关水泵
#define Plus  0xFFA857  // +  :开水泵
#define EQ    0xFF906F  //EQ  :水泵自动/手动模式切换
/**********0-9**********/
#define But_0 0xFF6897
#define But_1 0xFF30CF
#define But_2 0xFF18E7
#define But_3 0xFF7A85
#define But_4 0xFF10EF
#define But_5 0xFF38C7
#define But_6 0xFF5AA5
#define But_7 0xFF42BD
#define But_8 0xFF4AB5
#define But_9 0xFF52AD

/**********LCd界面**********/
#define Welcome      0              //欢迎界面
#define Show_Time    1              //时间显示界面
#define Temp_Humi    2              //温湿度显示界面
#define Soil_Pump    3              //土壤湿度、水泵界面
#define Time_Setting 4              //时间设定界面
#define Auto true                   //页面轮播
#define Maul false                  //手动翻页
boolean Auto_Maul_Flip_Mode = Maul; //默认为手动翻页
unsigned int Page = Welcome;        //默认主界面为欢迎界面

/**************************************************************************/

/***************************************************************************
 **********************************函数声明********************************
 **************************************************************************/
void Welcome_Page();                  //欢迎界面
void Show_Time_Page();                //时间显示界面
void Temp_Humi_Page();                //温湿度显示界面
void Soil_Pump_Page();                //土壤湿度、水泵界面
void Time_Setting_Page();             //时间设定界面

void IR_Control();                    //主要红外控制
void Get_Current_Time();              //获取当前时间，拼接字符串
void Temp_Humi_Get_Data();            //获取当前温湿度数据，拼接字符串
void Soil_Liquid_Get_Data();          //获取当前土壤湿度、液位、水泵控制模式，拼接字符串
void Pump_Mode_Control();             //水泵控制模式切换
void Pump_Auto_Control();             //自动模式下水泵控制
void Auto_Flip();                     //自动翻页计时操作
void Current_Auto_To_Maul();          //退出自动翻页
void Buzzer_Respond(int b_times);     //蜂鸣器响应
void Input_Clear(byte Col, byte Row); //清除LCD数据
void Clean_Buffer(char *Buf,int Len); //清空缓冲区

/***************************************************************************/


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);//initialize the serial

  //I2C_LCD1602   
  lcd.init();               //初始化LCD
  lcd.backlight();          //打开LCD背光
  lcd.noCursor();           //隐藏光标
  
  pinMode(Pump_Relay, OUTPUT);
  pinMode(Buzzer_Pin, OUTPUT);
  Buzzer_OFF;
  //DS1302
  rtc.write_protect(false); //关闭写保护
  rtc.halt(false);          //关闭睡眠

  irrecv.enableIRIn();      //使能红外接收模块
  Welcome_Page();           //欢迎界面
}

void loop() {
  // put your main code here, to run repeatedly:
  IR_Control();           //红外遥控操作
  Auto_Flip();            //自动翻页模式下的页面转换操作
  Soil_Liquid_Get_Data(); //数据获取
  Pump_Auto_Control();    //水泵自动控制模式的操作
}

/***************************************************************************
 *******************************主要红外控制*******************************
 **************************************************************************/
/**********主要的红外控制**********/
void IR_Control(){
  switch(Page){
    /**********欢迎页面"0"**********/
    case Welcome:      //欢迎页面"0"
      if (irrecv.decode(&results)){ //当接收到红外信号
        Pump_Mode_Control();
        if (results.value == PREV){ //上一页
          Buzzer_Respond(1);      //蜂鸣器响应
          Current_Auto_To_Maul(); //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Soil_Pump;
          lcd.clear();
        } else if (results.value == NEXT){ //下一页
          Buzzer_Respond(1);      //蜂鸣器响应
          Current_Auto_To_Maul(); //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Show_Time;
          lcd.clear();
          Show_Time_Page();
        } else if (results.value == PLAY && Auto_Maul_Flip_Mode == Maul){ //当前为手动翻页，按下自动翻页键(PLAY)，进入自动翻页
          Buzzer_Respond(2);      //蜂鸣器响应
          Auto_Maul_Flip_Mode = Auto;
          Last_Page_Rotation_Time = millis();
        } else if (results.value == PLAY && Auto_Maul_Flip_Mode == Auto){ //当前为自动翻页，按下自动翻页键(PLAY)，退出自动翻页
          Buzzer_Respond(1);      //蜂鸣器响应
          Auto_Maul_Flip_Mode = Maul;
        }
        irrecv.resume(); //等待接收下一次的红外信号
      }
      break;
      
    /**********显示时间"1"**********/
    case Show_Time:         //显示时间"1"
      Show_Time_Page();
      if (irrecv.decode(&results)){
        Pump_Mode_Control();
        if (results.value == PREV){ //上一页
          Buzzer_Respond(1);        //蜂鸣器响应
          Current_Auto_To_Maul();   //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Welcome;
          lcd.clear();
          Welcome_Page();
        } else if (results.value == NEXT){ //下一页
          Buzzer_Respond(1);        //蜂鸣器响应
          Current_Auto_To_Maul();   //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Temp_Humi;
          lcd.clear();
          //Temp_Humi_Page();
        } else if (results.value == CH){  //设定时间
          Buzzer_Respond(1);        //蜂鸣器响应
          Current_Auto_To_Maul();   //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Time_Setting;
          lcd.clear();
          Time_Setting_Page();      //时间设置界面
        } else if (results.value == PLAY && Auto_Maul_Flip_Mode == Maul){ //当前为手动翻页，按下自动翻页键(PLAY)，进入自动翻页
          Buzzer_Respond(2);        //蜂鸣器响应
          Auto_Maul_Flip_Mode = Auto;
          Last_Page_Rotation_Time = millis();
        } else if (results.value == PLAY && Auto_Maul_Flip_Mode == Auto){ //当前为自动翻页，按下自动翻页键(PLAY)，退出自动翻页
          Buzzer_Respond(1);        //蜂鸣器响应
          Auto_Maul_Flip_Mode = Maul;
        }
        irrecv.resume();
      }
      break;
      
    /**********温湿度显示界面"2"**********/
    case Temp_Humi:    //温湿度显示界面"2"
      Temp_Humi_Page();
      if (irrecv.decode(&results)){
        Pump_Mode_Control();
        if (results.value == PREV){ //上一页
          Buzzer_Respond(1);        //蜂鸣器响应
          Current_Auto_To_Maul();   //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Show_Time;
          lcd.clear();
        } else if (results.value == NEXT){ //下一页
          Buzzer_Respond(1);        //蜂鸣器响应
          Current_Auto_To_Maul();   //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Soil_Pump;
          lcd.clear();
        } else if (results.value == PLAY && Auto_Maul_Flip_Mode == Maul){ //当前为手动翻页，按下自动翻页键(PLAY)，进入自动翻页
          Buzzer_Respond(2);        //蜂鸣器响应
          Auto_Maul_Flip_Mode = Auto;
          Last_Page_Rotation_Time = millis();
        } else if (results.value == PLAY && Auto_Maul_Flip_Mode == Auto){ //当前为自动翻页，按下自动翻页键(PLAY)，退出自动翻页
          Buzzer_Respond(1);        //蜂鸣器响应
          Auto_Maul_Flip_Mode = Maul;
        }
        irrecv.resume();
      }
      break;
      
    /**********土壤湿度、水泵界面"3"**********/
    case Soil_Pump:    //土壤湿度、水泵界面"3"
      Soil_Pump_Page();
      if (irrecv.decode(&results)){
        Pump_Mode_Control();
        if (results.value == PREV){ //上一页
          Buzzer_Respond(1);        //蜂鸣器响应
          Current_Auto_To_Maul();   //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Temp_Humi;
          lcd.clear();
        } else if (results.value == NEXT){ //下一页
          Buzzer_Respond(1);        //蜂鸣器响应
          Current_Auto_To_Maul();   //自动翻页中，手动切换页面，则自动退出自动播放
          Page = Welcome;
          lcd.clear();
          Welcome_Page();
        } else if (results.value == PLAY && Auto_Maul_Flip_Mode == Maul){ //当前为手动翻页，按下自动翻页键(PLAY)，进入自动翻页
          Buzzer_Respond(2);        //蜂鸣器响应
          Auto_Maul_Flip_Mode = Auto;
          Last_Page_Rotation_Time = millis();
        } else if (results.value == PLAY && Auto_Maul_Flip_Mode == Auto){ //当前为自动翻页，按下自动翻页键(PLAY)，退出自动翻页
          Buzzer_Respond(1);        //蜂鸣器响应
          Auto_Maul_Flip_Mode = Maul;
        }
        irrecv.resume();
      }
      break;
      
    /**********时间设定界面**********/
    case Time_Setting: //时间设定界面
      if (irrecv.decode(&results)){
        if (results.value == CH_L){
          //CH-: 删除字符
          Buzzer_Respond(1);        //蜂鸣器响应
          InputCode.remove(InputCode.length()-1); //删除字符串最后一个字符

          //清除LCD中最后一个字符，当第1行的数据清空后，自动转为第0行的数据继续删除
          if (InputCode.length() < 5){
            //当前在第0行，若上面字符串删除字符后长度为4，则需要清除的字符在(15, 0)
            Current_ROWS = 0;
            lcd.setCursor(InputCode.length()+11, 0);
            lcd.print(" ");
            lcd.setCursor(InputCode.length()+11, 0);
          } else{
            //当前在第1行，若在LCD上已封顶，上面字符串删除字符后长度为20，则需要清除的字符在(15, 1)
            lcd.setCursor(InputCode.length()-5, 1);
            lcd.print(" ");
            lcd.setCursor(InputCode.length()-5, 1);
          }
        } else if(results.value == CH){
          //CH: 确定时间
          Buzzer_Respond(1);                //蜂鸣器响应
          Serial.print("You inputed : ");   //将输入的字符在串口中打印出来
          Serial.println(InputCode);
          //以"-"分割字符串
          for (int i = 0; i < InputCode.length(); i++){
            if (InputCode[i] == '-' || InputCode[i] == 0x10 || InputCode[i] == 0x13){  //0x10:转义字符、0x13:回车
              j++;
            } else{
              NumData[j] = NumData[j] * 10 + (InputCode[i] - '0');
            }
          }
          if(NumData[1]>0 && NumData[1]<13 && NumData[2]>0 && NumData[2]<32 && NumData[3]<24 && NumData[4]<60 && NumData[5]<60 && NumData[6]>0 && NumData[6]<8){
            //检查时间格式
            //年:NumData[0]，月:NumData[1]，日:NumData[2]，时:NumData[3]，分:NumData[4]，秒:NumData[5]，周几:NumData[6]
            Time t(NumData[0], NumData[1], NumData[2], NumData[3], NumData[4], NumData[5], NumData[6]); 
            rtc.time(t);          //将数据写入1302芯片
            Serial.println("Write Success!");
            Page = Show_Time;     //页面值置为Show_Time，返回时间显示界面
            lcd.noCursor();       //隐藏光标  
            lcd.clear();  
          } else{
            //时间格式有误，重新
            Serial.println("Incorrect time format!");
            Input_Clear(0, 1);    //清空第1行
            Input_Clear(11, 0);   //清空SettingTime后面的字符
          }
          //恢复关键变量值，，以便下一次输入
          Current_ROWS = 0;       //当前所在行恢复为第0行
          j = 0;                  //清空j计数
          InputCode = "";         //清空InputCode
          for(int i = 0; i < 7 ; i++) NumData[i]=0; //清空NumData
          
        } else if (results.value == CH_R){
          //退出时间设置
          Buzzer_Respond(1);      //蜂鸣器响应
          InputCode = "";
          Page = Show_Time;
          lcd.noCursor();
          lcd.clear();
        } //在LCD中打印相应的字符，当字符串长度达到了21，则不再继续添加、打印字符，控制时间格式；当第0行打印满了，自动转为第1行继续打印
          else if (results.value == But_0){if(InputCode.length() < 21){InputCode += '0'; lcd.print("0");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_1){if(InputCode.length() < 21){InputCode += '1'; lcd.print("1");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_2){if(InputCode.length() < 21){InputCode += '2'; lcd.print("2");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_3){if(InputCode.length() < 21){InputCode += '3'; lcd.print("3");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_4){if(InputCode.length() < 21){InputCode += '4'; lcd.print("4");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_5){if(InputCode.length() < 21){InputCode += '5'; lcd.print("5");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_6){if(InputCode.length() < 21){InputCode += '6'; lcd.print("6");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_7){if(InputCode.length() < 21){InputCode += '7'; lcd.print("7");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_8){if(InputCode.length() < 21){InputCode += '8'; lcd.print("8");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == But_9){if(InputCode.length() < 21){InputCode += '9'; lcd.print("9");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } else if (results.value == Minus){if(InputCode.length() < 21){InputCode += '-'; lcd.print("-");} Buzzer_Respond(1); if(InputCode.length()>4 && Current_ROWS == 0){lcd.setCursor(0, 1); Current_ROWS = 1;}
        } 
        irrecv.resume();
      }
      break;
  }
}

/***************************************************************************/

/***************************************************************************
 ***********************************界面**********************************
 **************************************************************************/
/**********欢迎界面**********/
void Welcome_Page(){
  /* ******************
   * 打印效果：
   * "   Welcome to   "
   * "  this device!  "
   ********************/
  lcd.setCursor(3, 0);
  lcd.print("Welcome to   ");
  lcd.setCursor(2, 1);
  lcd.print("this device!");
}

/**********时间显示界面**********/
void Show_Time_Page(){
  /* ******************
   * 打印效果：
   * " 2020-01-01 Fri "
   * "    00:00:00    "
   ********************/
  if (millis() - Timing >= 1000){ //每秒改变一次LCD上的时间
    Get_Current_Time();           //获取时间数据、拼接字符串
    Serial.println(SERIAL_BUF);   //在串口打印时间，eg: "Fri 2020-06-12 00:00:00"
    lcd.setCursor(1,0);           //光标设为1列0行
    lcd.print(LCD_BUF1);          //" 2020-01-01 Fri "
    lcd.setCursor(4,1);
    lcd.print(LCD_BUF2);          //"    00:00:00    "
    Timing = millis();            //重新计时
  }
}

/**********温湿度显示界面**********/
void Temp_Humi_Page(){
  /* ******************
   * 打印效果：
   * "Temp:25.0C      "
   * "Humi:10.0%      "
   ********************/
  Temp_Humi_Get_Data();   //获取当前时间数据、拼接字符串
  Serial.println(LCD_BUF1);
  Serial.println(LCD_BUF2);
  lcd.setCursor(0, 0);
  lcd.print(LCD_BUF1);
  lcd.setCursor(0, 1);
  lcd.print(LCD_BUF2);
}

/**********土壤湿度、液位、水泵界面**********/
void Soil_Pump_Page(){
  /* 打印效果：
   * "Soil :10%   Auto"
   * "Water:Low
   */
   Serial.println(Soil_Liquid_BUF1);
   Serial.println(Soil_Liquid_BUF2);
   lcd.setCursor(0, 0);
   lcd.print(Soil_Liquid_BUF1);
   lcd.setCursor(0, 1);
   lcd.print(Soil_Liquid_BUF2);
   delay(150); 
}

/**********时间设定界面**********/
void Time_Setting_Page(){
  /*
   * "SettingTime2020-" 
   * "01-01-00-00-00-1"
   */
  lcd.setCursor(0, 0);
  lcd.print("SettingTime");
  lcd.cursor();
}

/***************************************************************************/


/***************************************************************************
 ****************************获取数据、拼接字符串****************************
 **************************************************************************/
/**********获取当前时间数据、拼接字符串**********/
void Get_Current_Time(){
  Time t = rtc.time();            //获取当前DS1302芯片的时间
  memset(day, 0, sizeof(day));
  //获取星期几
  if (t.day == 1)      { strcpy(day, "Sun"); }
  else if (t.day == 2) { strcpy(day, "Mon"); }
  else if (t.day == 3) { strcpy(day, "Tue"); }
  else if (t.day == 4) { strcpy(day, "Wed"); }
  else if (t.day == 5) { strcpy(day, "Thu"); }
  else if (t.day == 6) { strcpy(day, "Fri"); }
  else if (t.day == 7) { strcpy(day, "Sat"); }
  //格式化时间和日期并插入临时缓冲区
  //串口打印的时间参数
  Clean_Buffer(SERIAL_BUF, SERIAL_BUF_LEN);
  snprintf(SERIAL_BUF, SERIAL_BUF_LEN, "%s %04d-%02d-%02d %02d:%02d:%02d", day, t.yr, t.mon, t.date, t.hr, t.min, t.sec);
  //LCD打印的时间参数
  Clean_Buffer(LCD_BUF1, BUF_LEN);
  Clean_Buffer(LCD_BUF2, BUF_LEN);
  snprintf(LCD_BUF1, BUF_LEN, "%04d-%02d-%02d %s", t.yr, t.mon, t.date, day);
  snprintf(LCD_BUF2, BUF_LEN, "%02d:%02d:%02d", t.hr, t.min, t.sec);
}

/**********获取当前温湿度数据、拼接字符串**********/
void Temp_Humi_Get_Data(){
  D: int chk = DHT.read11(DHT11_Pin);//read the value returned from sensor
  switch (chk){     //数据检错，过滤错误数据
    case DHTLIB_OK: 
      Serial.println("OK!");
      break;
    case DHTLIB_ERROR_CHECKSUM: 
      Serial.println("Checksum error,\t"); 
      break;
    case DHTLIB_ERROR_TIMEOUT: 
      goto D;
//      Serial.println("Time out error,\t"); 
      break;
    default: 
      goto D;
//      Serial.println("Unknown error,\t"); 
      break;
  }
  int Temp = DHT.temperature * 10;
  int Humi = DHT.humidity * 10;
  Clean_Buffer(LCD_BUF1, BUF_LEN);
  Clean_Buffer(LCD_BUF2, BUF_LEN);
  //snprintf不支持float格式数据，这里是将其*10转为整型，再分别提取整数、小数部分
  snprintf(LCD_BUF1, BUF_LEN, "Temp:%02d.%dC",  Temp/10, Temp%10);
  snprintf(LCD_BUF2, BUF_LEN, "Humi:%02d.%d%%", Humi/10, Humi%10);
}

/**********土壤、液位、水泵数据获取**********/
void Soil_Liquid_Get_Data(){
  AD_Soil_Value = 1023 - Soil_Read;                               //将土壤湿度的模拟量从1023-0反转为0-1023，反向转为正向
  Soil_Percentage = map(AD_Soil_Value, 0, 1023, 0, 100);          //将模拟量0-1023映射为0-100百分比，用于展示
  AD_Liquid_Level_Value = 1023 - Liquid_Level_Read;               //将液位的模拟量从1023-0反转为0-1023，反向转为正向
  Level_Percentage = map(AD_Liquid_Level_Value, 0, 1023, 0, 100); //将模拟量0-1023映射为0-100百分比，用于展示
  
  //根据当前水泵的控制模式来进行字符串拼接
  if (Pump_Mode == Auto){
    Clean_Buffer(Soil_Liquid_BUF1, BUF_LEN);
    snprintf(Soil_Liquid_BUF1, BUF_LEN, "Soil :%02d%%   Auto", Soil_Percentage);
  } else{ //Maul
    Clean_Buffer(Soil_Liquid_BUF1, BUF_LEN);
    snprintf(Soil_Liquid_BUF1, BUF_LEN, "Soil :%02d%%   Maul", Soil_Percentage);
  }
  
  //根据当前水位的情况来进行字符串拼接
  if (Level_Percentage <= 30){  //液位低于30%
    if(Liquid_Flag != Low){     //若当前液位标志不为Low
      Input_Clear(6, 1);        //清除LCD中液位的状态：防止字符串过长，影响下次显示
      Liquid_Flag = Low;        //将液位标志置为Low
    }
    Clean_Buffer(Soil_Liquid_BUF2, BUF_LEN);              //清空缓冲区
    snprintf(Soil_Liquid_BUF2, BUF_LEN, "Water:Low");     //拼接字符串
  } else if (Level_Percentage > 30 && Level_Percentage <= 60){  //液位在30-60%之间
    if(Liquid_Flag != Midium){  //若当前液位标志不为Midium
      Input_Clear(6, 1);        //清除LCD中液位的状态：防止字符串过长，影响下次显示
      Liquid_Flag = Midium;     //将液位标志置为Midium
    } 
    Clean_Buffer(Soil_Liquid_BUF2, BUF_LEN);              //清空缓冲区
    snprintf(Soil_Liquid_BUF2, BUF_LEN, "Water:Midium");  //拼接字符串
  } else{                       //液位高于60%
    if (Liquid_Flag != High){   //若当前液位标志不为High
      Input_Clear(6, 1);        //清除LCD中液位的状态：防止字符串过长，影响下次显示
      Liquid_Flag = High;       //将液位标志置为High
    }
    Clean_Buffer(Soil_Liquid_BUF2, BUF_LEN);              //清空缓冲区
    snprintf(Soil_Liquid_BUF2, BUF_LEN, "Water:High");    //拼接字符串
  }
}

/***************************************************************************/


/***************************************************************************
 *****************************自动/手动控制操作*****************************
 **************************************************************************/
/**********水泵模式切换**********/
void Pump_Mode_Control(){
  if (Pump_Mode == Auto){               //若当前为自动模式
    if (results.value == Minus || results.value == EQ){
      //按下"-"或"EQ"
      Buzzer_Respond(1);                //蜂鸣器响应
      Pump_Mode = Maul;                 //切换为手动模式
      Pump_OFF;                         //关闭水泵
    } else if (results.value == Plus){  //按下"+"
      Buzzer_Respond(1);
      Pump_Mode = Maul;
      Pump_ON;
    }
  } else{                               //若当前为手动模式
    if (results.value == Minus){
      Buzzer_Respond(1);
      Pump_OFF;
    } else if (results.value == Plus){
      Buzzer_Respond(1);
      Pump_ON;
    } else if (results.value == EQ){
      Buzzer_Respond(2);
      Pump_Mode = Auto;
    }
  }
}

/**********自动模式下水泵的控制**********/
void Pump_Auto_Control(){
  if (Pump_Mode == Auto && Soil_Percentage < 30){
    Pump_ON;
  } else if (Pump_Mode == Auto && Soil_Percentage >= 30){
    Pump_OFF;
  }
}

/**********自动翻页的计时操作**********/
void Auto_Flip(){
  if (Auto_Maul_Flip_Mode == Auto && millis() - Last_Page_Rotation_Time >= 5000){
    //自动播放中
    Last_Page_Rotation_Time = millis();
    lcd.clear();
    /*方式1*/
    switch (Page){
      case Welcome:   Page = Show_Time; break;
      case Show_Time: Page = Temp_Humi; break;
      case Temp_Humi: Page = Soil_Pump; break;
      case Soil_Pump: Page = Welcome;   Welcome_Page(); break;
    }
    /*方式2*/
//    if (Page < 4){  //
//      Page++;
//      if (Page == 4){
//        Page = 0;
//        Welcome_Page();
//      }
//    }    
  }
}

/**********退出自动翻页**********/
void Current_Auto_To_Maul(){
  if(Auto_Maul_Flip_Mode == Auto){
    //自动翻页中，手动切换页面，则自动退出自动播放
    Auto_Maul_Flip_Mode = Maul;
  }
}

/***************************************************************************/

/***************************************************************************
 ******************************回馈、缓存区操作*****************************
 ***************************************************************************/
/**********蜂鸣器短暂响应**********/
void Buzzer_Respond(int b_times){  //b_times为参数，响几次
  for(int i = 1;i <= b_times;i++){ 
    Buzzer_ON;  delay(50);
    Buzzer_OFF; delay(50);
  }
}

/**********批量清除LCD上的字符**********/
/*从Col列开始，从左到右清除Row行的数据*/
void Input_Clear(byte Col, byte Row){
  byte last = LCD_COLS - Col;     //计算出要清空多少内容  
  lcd.setCursor(Col, Row);        //将光标定位在目标列和目标行中
  for (byte i=0; i<last; i++){
    lcd.print(" ");               //在LCD中打印“空格”，以表示清除
  }
  if (Page == Time_Setting){
    lcd.setCursor(Col, Row);
    lcd.cursor();
  } else{
    lcd.setCursor(0, Row);        //将光标重新定位在目标列和目标行中
    lcd.noCursor();                   //显示光标
  }
}

/**********清空缓冲区**********/
void Clean_Buffer(char *Buf,int Len){
  for(int i = 0;i < Len;i++){
    Buf[i] = '\0';
  } 
}

/**************************************************************************/
