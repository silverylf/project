const int pinRelay = D10;//管脚D12连接到继电器模块的信号脚
const int s_pin = A0;
int val = 0;//创建val储存压力值

void setup()
{
  Serial.begin(9600);
  pinMode(s_pin,INPUT);//压力
  pinMode(pinRelay,OUTPUT);//设置pinRelay脚为输出状态
}
void loop()
{
  Serial.println(analogRead(s_pin));
  val=analogRead(s_pin);//读取模拟接口0 的值，并将其赋给val
  if(val > 500){
  digitalWrite(pinRelay,HIGH);
  delay(500);
  }else{
  digitalWrite(pinRelay,LOW);
  delay(500);
  }
}
  
