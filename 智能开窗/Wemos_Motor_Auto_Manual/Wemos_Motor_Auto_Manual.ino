#include <ESP8266WiFi.h>//安装esp8266arduino开发环境
static WiFiClient espClient;

#include <AliyunIoTSDK.h>//引入阿里云 IoT SDK
//需要安装crypto库、PubSubClient库

const int IN1 = D5;
const int IN2 = D6;
const int IN3 = D7;
const int IN4 = D8;

int mode_status1;
int Mode_Sta;
#define Manual  0
#define Auto    1
boolean Windows_Sta;
#define Open  1
#define Close 0
unsigned long last_time;

//正转顺序
const char tab1[] =
{
  0x01, 0x03, 0x02, 0x06, 0x04, 0x0c, 0x08, 0x09
};

//反转的顺序
const char tab2[] =
{
  0x01, 0x09, 0x08, 0x0c, 0x04, 0x06, 0x02, 0x03
};

//设置产品和设备的信息，从阿里云设备信息里查看
#define PRODUCT_KEY     "a1KsmB0ZUgU"//替换自己的PRODUCT_KEY
#define DEVICE_NAME     "Curtains"//替换自己的DEVICE_NAME
#define DEVICE_SECRET   "ec6ad9eb842ba21d12747ed141e32ab5"//替换自己的DEVICE_SECRET
#define REGION_ID       "cn-shanghai"//默认cn-shanghai

#define WIFI_SSID       "360wifi"//替换自己的WIFI
#define WIFI_PASSWD     "asd152183."//替换自己的WIFI

void setup() {
  Serial.begin(9600);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);

  StepMotorStop();//步进电机初始停止

  //连接到wifi
  wifiInit(WIFI_SSID, WIFI_PASSWD);

  //初始化 iot，需传入 wifi 的 client，和设备产品信息
  AliyunIoTSDK::begin(espClient, PRODUCT_KEY, DEVICE_NAME, DEVICE_SECRET, REGION_ID);
  last_time = millis();

  Windows_Sta = Close;
  AliyunIoTSDK::send("stepmotor", 0);
  AliyunIoTSDK::send("mode_set", 1);

  //绑定一个设备属性回调，当远程修改此属性，会触发LED函数
  AliyunIoTSDK::bindData("stepmotor", stepmotor_status);//“stepmotor”为标识符
  AliyunIoTSDK::bindData("mode_set", mode_status);//“mode_set”为标识符
}

void loop() {
  AliyunIoTSDK::loop();//必要函数

  if (millis() - last_time >= 2000) {
    if (mode_status1 == Auto) {
      if (digitalRead(D4) == 0) { //光敏传感器(白天）
        if (Windows_Sta == Close) {
          Windows_Sta = Open;
          Serial.println("自动开窗");
          AliyunIoTSDK::send("stepmotor", Open);
          ctlStepMotor(720, 1);//远程开窗
          StepMotorStop();
        }
      } else {  //晚上(自动关窗)
        if (Windows_Sta == Open) {
          Windows_Sta = Close;
          Serial.println("自动关窗");
          AliyunIoTSDK::send("stepmotor", Close);
          ctlStepMotor(-720, 1);//远程关窗
          StepMotorStop();
        }
      }
    }
  }
}

//wifi 连接
void wifiInit(const char *ssid, const char *passphrase)
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passphrase);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("WiFi not Connect");
  }
  Serial.println("Connected to AP");
}

/*
   在手动控制状态下，则可对下发的指令作出反应，在自动控制状态下，无响应
*/
//电机的属性修改的回调函数
void stepmotor_status(JsonVariant L) {    //固定格式，修改参数s，内的“ ”
  int stepmotor_state = L["stepmotor"];   //参数l，“stepmotor”为标识符
  Serial.print("收到stepmotor："); Serial.println(stepmotor_state);
  if (mode_status1 == Manual) {           //当前模式为手动
    if (stepmotor_state == 1) {           //收到指令为1
      Serial.println("远程开窗");
      AliyunIoTSDK::send("stepmotor", 1);
      ctlStepMotor(720, 1);               //正转开窗
      StepMotorStop();
      //      delay(1000);
    } else {                              //收到指令不为1，（0）
      Serial.println("远程关窗");
      AliyunIoTSDK::send("stepmotor", 0);
      ctlStepMotor(-720, 1);//远程关窗
      StepMotorStop();
      //      delay(1000);
    }
  } else {
    //当前为自动模式下，无法进行
  }
}

/*
   自动模式、手动模式
   这块有问题，自动的应该放在loop里
*/
//模式的属性修改的回调函数
void mode_status(JsonVariant L)//固定格式，修改参数m，内的“ ”
{
  //收到的参数为手动(0)/自动(1)控制
  int mode_state = L["mode_set"];//参数l，“mode_set”为标识符
  mode_status1 = mode_state;
  Serial.printf("收到mode_status1："); Serial.println(mode_status1);
  AliyunIoTSDK::send("mode_set", mode_status1);
}

void ctlStepMotor(int angle, char speeds ) {  //控制步进电机正反转，角度>0正转，角度<0反转
  for (int j = 0; j < abs(angle) ; j++) {
    if (angle > 0) {  //正转开窗
      for (int i = 0; i < 8; i++) {
        digitalWrite(IN1, ((tab1[i] & 0x01) == 0x01 ? true : false));
        digitalWrite(IN2, ((tab1[i] & 0x02) == 0x02 ? true : false));
        digitalWrite(IN3, ((tab1[i] & 0x04) == 0x04 ? true : false));
        digitalWrite(IN4, ((tab1[i] & 0x08) == 0x08 ? true : false));
        delay(speeds);
      }
    } else {          //反转关窗
      for (int i = 0; i < 8 ; i++) {
        digitalWrite(IN1, ((tab2[i] & 0x01) == 0x01 ? true : false));
        digitalWrite(IN2, ((tab2[i] & 0x02) == 0x02 ? true : false));
        digitalWrite(IN3, ((tab2[i] & 0x04) == 0x04 ? true : false));
        digitalWrite(IN4, ((tab2[i] & 0x08) == 0x08 ? true : false));
        delay(speeds);
      }
    }
  }
}

//停止步进电机
void StepMotorStop() {
  digitalWrite(IN1, 0);
  digitalWrite(IN2, 0);
  digitalWrite(IN3, 0);
  digitalWrite(IN4, 0);
}
