//调用库文件
#include <Wire.h> //1602LCD屏
#include <LiquidCrystal_I2C.h>  //I²C转接板
#include <dht.h>  //DHT11温湿度传感器

//定义针脚
const int pumpin = 5; //pump（水泵）
const int fanpin = 7; //风扇
const int dht_pin = 8;//温湿度
const int ledPin = 11;//灯泡
const int ldrPin = A0;//光敏电阻

#define PIN_AO A2   //土壤湿度的模拟接口（检测干旱程度）
#define PIN_DO 4    //土壤湿度的数字借口（判断是否缺水）

//0x27是I²C的地址，可显示16列*2行的字符
LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27  0x3F for a 16 chars and 2 line display
dht DHT;  //创建一个变量指向对象

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);     //初始化串口通讯
  pinMode(ledPin, OUTPUT);//灯
  pinMode(ldrPin, INPUT);//光照
  pinMode(PIN_AO, INPUT);//土壤湿度
  pinMode(PIN_DO, INPUT);//土是否缺水
  pinMode(pumpin,OUTPUT);//水泵继电器
  pinMode(fanpin,OUTPUT);//风扇继电器
  lcd.init();  //initialize the lcd 初始化lcd屏
  lcd.backlight();  //open the backlight 打开lcd背光
}

void loop() {

  //光控灯
  int ldrStatus = analogRead(ldrPin);//用变量ldrStatus存储光敏电阻的值（光强越大，值越小，相当于阻值）
  if (ldrStatus >= 400) {     //读取值大于400，开灯
    digitalWrite(ledPin, HIGH);
    Serial.print("Its DARK, Turn on the LED : ");
    Serial.println(ldrStatus);
  } else {
    digitalWrite(ledPin, LOW);//否则，关灯
    Serial.print("Its BRIGHT, Turn off the LED : ");
    Serial.println(ldrStatus);
  }

  //水泵
  Serial.print("AO=");  
  Serial.print(analogRead(PIN_AO));//串口输出湿度值
  Serial.print("    DO=");  
  Serial.println(digitalRead(PIN_DO));
  if (PIN_AO > 800 && PIN_DO == 1){ //这个模拟值我测试的时候用水来测试的，好像调大了
    //干旱，开水泵
    digitalWrite(pumpin,LOW);//水泵打开
    Serial.println("开开开开开开开开开开开水泵");
  }
  else{//不干旱，不开水泵
    digitalWrite(pumpin,HIGH);//水泵关闭
    Serial.println("关关关关关关关关关关关水泵");
  }
//  delay(500); 
  
  //温控风扇
  DHT.read11(dht_pin);
  Serial.print("humidity = ");//湿度
  Serial.print(DHT.humidity);
  Serial.print("%  ");
 
  Serial.print("temperature = ");//温度
  Serial.print(DHT.temperature);
  Serial.println("C  ");
  //温度高于30℃，风扇散热
  if(DHT.temperature>=30){
    digitalWrite(fanpin,LOW);//继电器控制风扇打开
    Serial.println("开开开开开开开开开开开风扇");
  }
  else{
    digitalWrite(fanpin,HIGH);//继电器控制风扇关闭
    Serial.println("关关关关关关关关关关关风扇");
  }
  lcd.setCursor(0,0); //将光标定在0列0行
  lcd.print("Tem:");  //LCD输出"Tem:"
  lcd.print(DHT.temperature);//接着输出温度值
  lcd.print("C");     //接着输出单位
  lcd.setCursor(0,1); //将光标定在0列1行
  lcd.print("Soil:"); //LCD输出"Soil:"
  lcd.print(analogRead(PIN_AO));//输出土壤湿度
  lcd.print(" LED:"); //接着输出"LED:"
  //输出目前LED的状态
  if(digitalRead(ledPin)==HIGH){
    lcd.print("ON");
  }
  else
    lcd.print("OFF");
      delay(1000);

  lcd.clear(); //清屏，以实时更新数据
}
