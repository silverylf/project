#include <Wire.h>
//#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_CCS811.h>

Adafruit_CCS811 ccs;
unsigned  ccs811_status;
  
void setup() {
  Serial.begin(9600);
  Serial.println(F("CCS811 test"));
  
  if (!ccs.begin(0x5A)) {  
    Serial.println("Could not find a valid CCS811 sensor, check wiring!");
    while (1);
  }

  while(!ccs.available());
  float temp = ccs.calculateTemperature();
  ccs.setTempOffset(temp - 25.0);
}
  
void loop() {
    if(ccs.available()){
      if(!ccs.readData()){

    Serial.print("eCO2 = ");
    Serial.print(ccs.geteCO2());
    Serial.println(" ");
 
    Serial.print("TVOC = ");
    Serial.print(ccs.getTVOC());
    Serial.println(" ");
      }
    }
    Serial.println();
    delay(2000);
}
