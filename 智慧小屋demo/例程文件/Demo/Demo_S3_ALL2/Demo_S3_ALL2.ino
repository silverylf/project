/*
 * 程序说明：物模型Fan设置int32,0-255，读写；
 * 灯Light设置布尔 0-1，读写；
 */


#include <Wire.h>             //IIC库
#include <Adafruit_Sensor.h>  //通用传感器库
#include <Adafruit_BME280.h>  //BME280库
#include <Adafruit_CCS811.h>  //CCS811库

//User Modified Part
#define wifi_ssid     "TP-LINK_JINSUN"     //WIFI热点
#define wifi_psw      "loudongwu1234"      //WIFI热点密码
#define clientIDstr   "example"            //以下阿里云三元组
#define timestamp     "999"
#define ProductKey    "a1MvVXk7eAa"
#define DeviceName    "Smart_House"
#define DeviceSecret  "zv9pdmWFSkMzTlTPEYw0R2AbhTB1FD5X"
#define password      "b63af4f33aa9959e4ea76985e03525b2099d874a"



//Logic Preset
#define OFF           0
#define ON            1
#define MUTE          2
#define KEEP_OFF      2
#define KEEP_ON       3

//开关宏定义
#define AC_ON   digitalWrite(ACPin,HIGH)
#define AC_OFF  digitalWrite(ACPin,LOW)

#define Fan_ON(X)      analogWrite(FanPin,X)
#define Fan_OFF     analogWrite(FanPin,0)

#define Buzzer_ON   digitalWrite(BuzzerPin,HIGH)
#define Buzzer_OFF  digitalWrite(BuzzerPin,LOW)

#define Pump_ON     digitalWrite(PumpPin,HIGH)
#define Pump_OFF    digitalWrite(PumpPin,LOW)

//阈值设定
#define AC_ON_val     (27.00)
#define AC_OFF_val    (25.50)
#define Gas_ON_val      500//400
#define Gas_OFF_val     300//150
#define Light_ON_val      400
#define Light_OFF_val     200
#define Pump_ON_val       900
#define Pump_OFF_val      600
#define eCO2_ON_val        900
#define eCO2_OFF_val       800

//Status Pool状态变量
float RoomTemp;
int   RoomHumi;
int   AC = OFF;
int   Buzzer = OFF;
int   GasDetector = 0;
int   Fan = 0;
int   Fan_Set=200;
int   LightDetector = 0;
int   Light = OFF;
int   Curtain = ON;
int   SoilHumi = 0;
int   Pump = OFF;
int   eCO2 = 0;
int   TVOC = 0;

//ATcmd Format  AT命令格式
#define AT                    "AT\r\n"
#define AT_OK                 "OK"
#define AT_REBOOT             "AT+REBOOT\r\n"
#define AT_ECHO_OFF           "AT+UARTE=OFF\r\n"
#define AT_MSG_ON             "AT+WEVENT=ON\r\n"

#define AT_WIFI_START         "AT+WJAP=%s,%s\r\n"
#define AT_WIFI_START_SUCC    "+WEVENT:STATION_UP"

#define AT_MQTT_AUTH          "AT+MQTTAUTH=%s&%s,%s\r\n"
#define AT_MQTT_CID           "AT+MQTTCID=%s|securemode=3\\,signmethod=hmacsha1\\,timestamp=%s|\r\n"
#define AT_MQTT_SOCK          "AT+MQTTSOCK=%s.iot-as-mqtt.cn-shanghai.aliyuncs.com,1883\r\n"

#define AT_MQTT_AUTOSTART_OFF "AT+MQTTAUTOSTART=OFF\r\n"
#define AT_MQTT_ALIVE         "AT+MQTTKEEPALIVE=500\r\n"
#define AT_MQTT_START         "AT+MQTTSTART\r\n"
#define AT_MQTT_START_SUCC    "+MQTTEVENT:CONNECT,SUCCESS"
#define AT_MQTT_PUB_SET       "AT+MQTTPUB=/sys/%s/%s/thing/event/property/post,1\r\n"
#define AT_MQTT_PUB_ALARM_SET "AT+MQTTPUB=/sys/%s/%s/thing/event/GasAlarm/post,1\r\n"
#define AT_MQTT_PUB_DATA      "AT+MQTTSEND=%d\r\n"
#define JSON_DATA_PACK        "{\"id\":\"102\",\"version\":\"1.0\",\"method\":\"thing.event.property.post\",\"params\":{\"RoomTemp\":%d.%02d,\"RoomHumi\":%d,\"AC\":%d,\"Fan\":%d,\"Buzzer\":%d,\"GasDetector\":%d}}\r\n"
#define JSON_DATA_PACK_2      "{\"id\":\"110\",\"version\":\"1.0\",\"method\":\"thing.event.property.post\",\"params\":{\"LightDetector\":%d,\"Curtain\":%d,\"Light\":%d,\"SoilHumi\":%d,\"Pump\":%d,\"eCO2\":%d,\"TVOC\":%d}}\r\n"
#define JSON_DATA_PACK_ALARM  "{\"id\":\"110\",\"version\":\"1.0\",\"method\":\"thing.event.GasAlarm.post\",\"params\":{\"GasDetector\":%d}}\r\n"
#define AT_MQTT_PUB_DATA_SUCC "+MQTTEVENT:PUBLISH,SUCCESS"
#define AT_MQTT_UNSUB         "AT+MQTTUNSUB=2\r\n"
#define AT_MQTT_SUB           "AT+MQTTSUB=2,/sys/%s/%s/thing/service/property/set,1\r\n"
#define AT_MQTT_SUB_SUCC      "+MQTTEVENT:2,SUBSCRIBE,SUCCESS"

#define AT_BUZZER                "\"Buzzer\":"
#define AT_BUZZER_Alarm          "\"Buzzer\":1"
#define AT_BUZZER_MUTE           "\"Buzzer\":2"
#define AT_RoomTemp_ON_SET       "\"RoomTemp\":"
#define AT_RoomTemp_OFF_SET      "\"RoomTemp\":"
#define AT_Fan                   "\"Fan\":"
#define AT_Light                 "\"Light\":"
#define AT_RX_END                "\"1.0.0\"}"


//Pin Map
#define ACPin                 2
#define BuzzerPin             3
#define PumpPin               4
#define CurtainOpenPin        5
#define CurtainClosePin       6
#define Light1Pin             7
#define Light2Pin             8
#define Light3Pin             9
#define FanPin                11

#define GasPin                A0
#define SoilHumiPin           A1
#define LightPin              A2

//缓冲区定义
#define DEFAULT_TIMEOUT       10   //秒，超时设定
#define BUF_LEN               200  //缓冲区长度宏定义
#define BUF_LEN_DATA          200  //

char      ATcmd[BUF_LEN];          //AT指令缓冲区
char      ATbuffer[BUF_LEN];       //数据接收缓冲区
char      ATdata[BUF_LEN_DATA];    //数据发送缓冲区



Adafruit_BME280 bme;               //BME280对象
unsigned  bme280_status;           //状态变量
Adafruit_CCS811 ccs;               //CCS811对象
unsigned  ccs811_status;           //状态变量

void setup() {
  //Serial Initial
  Serial3.begin(115200);
  Serial.begin(115200);
  //Pin Initial
  Pin_init();

  //Sensor Initial

  bme280_status = bme.begin(0x76);
  while(!bme280_status)bme280_status = bme.begin(0x76);

  ccs811_status = ccs.begin(0x5A);
  while(!ccs811_status)ccs811_status = ccs.begin(0x5A);

  while(!ccs.available());
  float temp = ccs.calculateTemperature();
  ccs.setTempOffset(temp - 25.0);

  BEEP(1);

  //Cloud Initial
  while (1)
  {
    if (!WiFi_init())continue;
    BEEP(2);
    if (!Ali_connect())continue;
    break;
  }
  BEEP(3);
}

void loop() {

  //SensorCollect
  RoomTemp = bme.readTemperature();
  RoomHumi = bme.readHumidity();
  GasDetector = analogRead(GasPin);
  LightDetector = analogRead(LightPin);
  SoilHumi = analogRead(SoilHumiPin);

  if(ccs.available())
  {
    if(!ccs.readData())
    {
      eCO2 = ccs.geteCO2();
      TVOC = ccs.getTVOC();
    }
  }

  //Logic Process
  //1.RoomTemp
  if ((RoomTemp > AC_ON_val) && (AC == OFF))
  {
    AC = ON;
    AC_ON;
  }
  if ((RoomTemp < AC_OFF_val) && (AC == ON))
  {
    AC = OFF;
    AC_OFF;
  }
  //2.Gas&PM2.5
  if(((GasDetector > Gas_ON_val)||(eCO2 > eCO2_ON_val))&&(Fan == 0))
  //if ((GasDetector > Gas_ON_val) && (Fan == OFF))
  {
    Fan = Fan_Set;
    Fan_ON(Fan);
  }
  if ((GasDetector > Gas_ON_val) && (Buzzer == OFF))
  {
    Buzzer = ON;
    Buzzer_ON;

    cleanBuffer(ATcmd, BUF_LEN);
    snprintf(ATcmd, BUF_LEN, AT_MQTT_PUB_ALARM_SET, ProductKey, DeviceName);
    bool mainflag = check_send_cmd(ATcmd, AT_OK, DEFAULT_TIMEOUT);

    cleanBuffer(ATdata, BUF_LEN_DATA);
    int mainlen = snprintf(ATdata, BUF_LEN_DATA, JSON_DATA_PACK_ALARM, GasDetector);

    cleanBuffer(ATcmd, BUF_LEN);
    snprintf(ATcmd, BUF_LEN, AT_MQTT_PUB_DATA, mainlen - 1);
    mainflag = check_send_cmd(ATcmd, ">", DEFAULT_TIMEOUT);
    if (mainflag) mainflag = check_send_cmd(ATdata, AT_MQTT_PUB_DATA_SUCC, 20);
  }
  if ((GasDetector < Gas_OFF_val) && (Buzzer != OFF))
  {
    Buzzer = OFF;
    Buzzer_OFF;
  }
  if((GasDetector < Gas_OFF_val)&&(eCO2 < eCO2_OFF_val)&&(Fan != 0))
  //if ((GasDetector < Gas_OFF_val) && (Fan == ON))
  {
    Fan = 0;
    Fan_OFF;
  }
  //3.Light
  if ((LightDetector > Light_ON_val) && (Curtain == ON))
  {
    Curtain = OFF;
    Curtain_OFF();
    Light_ON();
    Light = ON;
  }

  if ((LightDetector < Light_OFF_val) && (Curtain == OFF))
  {
    Curtain = ON;
    Curtain_ON();
    Light_OFF();
    Light = OFF;
  }
  //4.SoilHumi
  if ((SoilHumi > Pump_ON_val) && (Pump == OFF))
  {
    Pump = ON;
    Pump_ON;
  }
  if ((SoilHumi < Pump_OFF_val) && (Pump == ON))
  {
    Pump = OFF;
    Pump_OFF;
  }

  //Upload
  Upload();

  //MsgReceive
  if (check_send_cmd2(AT, AT_RX_END, DEFAULT_TIMEOUT)) 
  {
    char *c,*c2;
    char x;
    int n,i;
    int Fan_Val;
    c=strstr(ATbuffer,AT_BUZZER);//蜂鸣器操作
    if(c!=NULL)
    {
      Serial.println(strlen(c));
      x=*(c+9);
      Serial.println(x);
      if(x=='0')
      {
        BEEP(0);
      }
      if(x=='1')
      {
        BEEP(1);
      }
      if(x=='2')
      {
        Buzzer_mute();
      }      
    }
    c=strstr(ATbuffer,AT_Light);  //灯操作
    if(c!=NULL)
    {
      Serial.println(c);
      x=*(c+8);
      Serial.println(x);
      if(x=='0')
      {
        Light_OFF();
        Light = OFF;
      }
      if(x=='1')
      {
        Light_ON();
        Light = ON;
      }     
    }
    c=strstr(ATbuffer,AT_Fan);  //风扇PWM输出操作
    if(c!=NULL)
    {
      Serial.println(c);
      c2=strchr(c,'}');
      n=strlen(c)-strlen(c2);
      Serial.println(n);
      Fan_Val=0;  
      for(i=0;i<n-6;i++)
      {
        if(i>0)
          Fan_Val=Fan_Val*10;
        Fan_Val+=*(c+6+i)-0x30;
      }
      Fan_Set=Fan_Val;
      if(Fan!=0)
      {
        Fan=Fan_Set;
        Fan_ON(Fan);
      }
      Serial.println(Fan_Set);
    }
  }
  //if (check_send_cmd2(AT, AT_RoomTemp_ON_SET, DEFAULT_TIMEOUT))   ; 
  //if (check_send_cmd2(AT, AT_BUZZER_MUTE, DEFAULT_TIMEOUT))Buzzer_mute();
  
}

bool Upload()
{
  bool flag;
  int inte1, frac1;
  int len;

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_MQTT_PUB_SET, ProductKey, DeviceName);
  flag = check_send_cmd(ATcmd, AT_OK, DEFAULT_TIMEOUT);


  cleanBuffer(ATdata, BUF_LEN_DATA);

  inte1 = (int)(RoomTemp);
  frac1 = (RoomTemp - inte1) * 100;

  len = snprintf(ATdata, BUF_LEN_DATA, JSON_DATA_PACK, inte1, frac1, RoomHumi, AC, Fan, Buzzer, GasDetector);

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_MQTT_PUB_DATA, len - 1);
  flag = check_send_cmd(ATcmd, ">", DEFAULT_TIMEOUT);
  if (flag) flag = check_send_cmd(ATdata, AT_MQTT_PUB_DATA_SUCC, 20);


  //  delay(500);

  cleanBuffer(ATdata, BUF_LEN_DATA);
  len = snprintf(ATdata, BUF_LEN_DATA, JSON_DATA_PACK_2, LightDetector, Curtain, Light, SoilHumi, Pump, eCO2, TVOC);

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_MQTT_PUB_DATA, len - 1);
  flag = check_send_cmd(ATcmd, ">", DEFAULT_TIMEOUT);
  if (flag) flag = check_send_cmd(ATdata, AT_MQTT_PUB_DATA_SUCC, 20);

  return flag;
}

bool Ali_connect()
{
  bool flag;

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_MQTT_AUTH, DeviceName, ProductKey, password);
  flag = check_send_cmd(ATcmd, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_MQTT_CID, clientIDstr, timestamp);
  flag = check_send_cmd(ATcmd, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_MQTT_SOCK, ProductKey);
  flag = check_send_cmd(ATcmd, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  flag = check_send_cmd(AT_MQTT_AUTOSTART_OFF, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  flag = check_send_cmd(AT_MQTT_ALIVE, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  flag = check_send_cmd(AT_MQTT_START, AT_MQTT_START_SUCC, 20);
  if (!flag)return false;

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_MQTT_PUB_SET, ProductKey, DeviceName);
  flag = check_send_cmd(ATcmd, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  //flag = check_send_cmd(AT_MQTT_UNSUB,AT_OK,DEFAULT_TIMEOUT);
  //if(!flag)return false;

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_MQTT_SUB, ProductKey, DeviceName);
  flag = check_send_cmd(ATcmd, AT_MQTT_SUB_SUCC, DEFAULT_TIMEOUT);
  if (!flag)BEEP(4);
  return flag;
}

bool WiFi_init()
{
  bool flag;

  flag = check_send_cmd(AT, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  flag = check_send_cmd(AT_REBOOT, AT_OK, 20);
  if (!flag)return false;
  delay(5000);

  flag = check_send_cmd(AT_ECHO_OFF, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  flag = check_send_cmd(AT_MSG_ON, AT_OK, DEFAULT_TIMEOUT);
  if (!flag)return false;

  cleanBuffer(ATcmd, BUF_LEN);
  snprintf(ATcmd, BUF_LEN, AT_WIFI_START, wifi_ssid, wifi_psw);
  flag = check_send_cmd(ATcmd, AT_WIFI_START_SUCC, 20);
  return flag;
}

bool check_send_cmd(const char* cmd, const char* resp, unsigned int timeout)
{
  int i = 0;
  unsigned long timeStart;
  timeStart = millis();
  cleanBuffer(ATbuffer, BUF_LEN);
  Serial3.print(cmd);//发送AT，
  Serial.println("发送1：");
  Serial.print(cmd);
  Serial3.flush();
  while (1)
  {
    while (Serial3.available())
    {
      ATbuffer[i++] = Serial3.read();
      if (i >= BUF_LEN)i = 0;
    }
    if (NULL != strstr(ATbuffer, resp))break;
    if ((unsigned long)(millis() - timeStart > timeout * 1000)) break;
  }

  if (NULL != strstr(ATbuffer, resp))
  {
    Serial.print(ATbuffer);
    Serial.println("接收1结束");
    return true;
  }
  return false;
}

bool check_send_cmd2(const char* cmd, const char* resp, unsigned int timeout)
{
  int i = 0;
  int j = 0;
  unsigned long timeStart;
  timeStart = millis();
  cleanBuffer(ATbuffer, BUF_LEN);
  Serial3.print(cmd);
  Serial.println("发送2：");
  Serial.print(cmd);
  Serial3.flush();
  while (1)
  {
    while (Serial3.available())
    {
      ATbuffer[i++] = Serial3.read();
      if (i >= BUF_LEN)i = 0;
    }
    /*
    if(i==101)
    {

      Serial3.flush();
      while (Serial3.available())
      {
        ATbuffer[i++] = Serial3.read();
        if (i >= BUF_LEN)i = 0;        
      }
    }
    */

    if (NULL != strstr(ATbuffer, resp))
    {
      break;
    }
    if ((unsigned long)(millis() - timeStart > timeout * 1000)) break;
  }

  if (NULL != strstr(ATbuffer, resp))
  {
    Serial.print(ATbuffer);
    Serial.print(i);
    Serial.println("接收2结束");
    
    return true;
  }
  return false;
}

void cleanBuffer(char *buf, int len)
{
  for (int i = 0; i < len; i++)
  {
    buf[i] = '\0';
  }
}

void Pin_init()
{
  pinMode(ACPin, OUTPUT);
  digitalWrite(ACPin, LOW);
  pinMode(BuzzerPin, OUTPUT);
  digitalWrite(BuzzerPin, LOW);
  pinMode(PumpPin, OUTPUT);
  digitalWrite(PumpPin, LOW);
  pinMode(CurtainOpenPin, OUTPUT);
  digitalWrite(CurtainOpenPin, LOW);
  pinMode(CurtainClosePin, OUTPUT);
  digitalWrite(CurtainClosePin, LOW);
  
  pinMode(Light1Pin, OUTPUT);
  analogWrite(Light1Pin, 0);
  pinMode(Light2Pin, OUTPUT);
  analogWrite(Light2Pin, 0);
  pinMode(Light3Pin, OUTPUT);
  analogWrite(Light3Pin, 0);
  
  pinMode(FanPin, OUTPUT);
  analogWrite(FanPin, Fan);
  Curtain_ON();
}

void BEEP(int b_time)
{
  for (int i = 1; i <= b_time; i++)
  {
    digitalWrite(BuzzerPin, HIGH);
    delay(100);
    digitalWrite(BuzzerPin, LOW);
    delay(100);
  }
}
void Buzzer_mute()
{
  Buzzer_OFF;
  Buzzer = MUTE;
}
void Curtain_ON()
{
  digitalWrite(CurtainOpenPin, HIGH);
  delay(200);
  digitalWrite(CurtainOpenPin, LOW);
}

void Curtain_OFF()
{
  digitalWrite(CurtainClosePin, HIGH);
  delay(200);
  digitalWrite(CurtainClosePin, LOW);
}

/*
void Light_ON()
{
  analogWrite(Light1Pin,10);
  digitalWrite(Light2Pin, HIGH);
  digitalWrite(Light3Pin, HIGH);
}

void Light_OFF()
{
  analogWrite(Light1Pin, 0);
  digitalWrite(Light2Pin, LOW);
  digitalWrite(Light3Pin, LOW);
}
*/
void Light_ON()
{
  analogWrite(Light1Pin,random(256));
  analogWrite(Light2Pin,random(256));
  analogWrite(Light3Pin,random(256));
}

void Light_OFF()
{
  analogWrite(Light1Pin, 0);
  analogWrite(Light2Pin, 0);
  analogWrite(Light3Pin, 0);
}
